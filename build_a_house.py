import os
import sys
import os.path as path

sys.path.append(path.join(path.expanduser('~'), "mcpipy"))
sys.path.append("c:/mcpipy")

import mcpi.minecraft as minecraft
import mcpi.block as block
mc = minecraft.Minecraft.create(address="192.168.1.110")

"""
建一栋房子。运行方法：

    python build_a_house.py
"""

# 以下为实际的操作内容
position = mc.player.getPos()
housePosition = position
housePosition.x = housePosition.x + 5
housePosition.z = housePosition.z + 5
mc.setBlocks(housePosition.x, housePosition.y, housePosition.z,
             housePosition.x + 6, housePosition.y + 6, housePosition.z + 6,
             block.COBBLESTONE)
mc.setBlocks(housePosition.x + 1, housePosition.y    , housePosition.z + 1,
             housePosition.x + 5, housePosition.y + 5, housePosition.z + 5,
             block.AIR)

mc.setBlock(housePosition.x + 1, housePosition.y + 5, housePosition.z, block.GLASS)
mc.setBlock(housePosition.x + 1, housePosition.y + 4, housePosition.z, block.GLASS)
mc.setBlock(housePosition.x + 2, housePosition.y + 5, housePosition.z, block.GLASS)
mc.setBlock(housePosition.x + 2, housePosition.y + 4, housePosition.z, block.GLASS)
mc.setBlock(housePosition.x + 4, housePosition.y + 5, housePosition.z, block.GLASS)
mc.setBlock(housePosition.x + 4, housePosition.y + 4, housePosition.z, block.GLASS)
mc.setBlock(housePosition.x + 5, housePosition.y + 5, housePosition.z, block.GLASS)
mc.setBlock(housePosition.x + 5, housePosition.y + 4, housePosition.z, block.GLASS)

mc.setBlock(housePosition.x + 3, housePosition.y + 0, housePosition.z , block.AIR)
mc.setBlock(housePosition.x + 3, housePosition.y + 1, housePosition.z , block.AIR)

# Another side of the house
mc.setBlock(housePosition.x, housePosition.y + 5, housePosition.z + 1, block.GLASS)
mc.setBlock(housePosition.x, housePosition.y + 4, housePosition.z + 1, block.GLASS)
mc.setBlock(housePosition.x, housePosition.y + 5, housePosition.z + 2, block.GLASS)
mc.setBlock(housePosition.x, housePosition.y + 4, housePosition.z + 2, block.GLASS)
mc.setBlock(housePosition.x, housePosition.y + 5, housePosition.z + 4, block.GLASS)
mc.setBlock(housePosition.x, housePosition.y + 4, housePosition.z + 4, block.GLASS)
mc.setBlock(housePosition.x, housePosition.y + 5, housePosition.z + 5, block.GLASS)
mc.setBlock(housePosition.x, housePosition.y + 4, housePosition.z + 5, block.GLASS)

# Another side of the house
mc.setBlock(housePosition.x + 6, housePosition.y + 5, housePosition.z + 1, block.GLASS)
mc.setBlock(housePosition.x + 6, housePosition.y + 4, housePosition.z + 1, block.GLASS)
mc.setBlock(housePosition.x + 6, housePosition.y + 5, housePosition.z + 2, block.GLASS)
mc.setBlock(housePosition.x + 6, housePosition.y + 4, housePosition.z + 2, block.GLASS)
mc.setBlock(housePosition.x + 6, housePosition.y + 5, housePosition.z + 4, block.GLASS)
mc.setBlock(housePosition.x + 6, housePosition.y + 4, housePosition.z + 4, block.GLASS)
mc.setBlock(housePosition.x + 6, housePosition.y + 5, housePosition.z + 5, block.GLASS)
mc.setBlock(housePosition.x + 6, housePosition.y + 4, housePosition.z + 5, block.GLASS)

# Very large windows on the last side!
mc.setBlock(housePosition.x + 1, housePosition.y + 5, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 1, housePosition.y + 4, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 1, housePosition.y + 3, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 1, housePosition.y + 2, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 2, housePosition.y + 5, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 2, housePosition.y + 4, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 2, housePosition.y + 3, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 2, housePosition.y + 2, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 4, housePosition.y + 5, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 4, housePosition.y + 4, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 4, housePosition.y + 3, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 4, housePosition.y + 2, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 5, housePosition.y + 5, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 5, housePosition.y + 4, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 5, housePosition.y + 3, housePosition.z + 6, block.GLASS)
mc.setBlock(housePosition.x + 5, housePosition.y + 2, housePosition.z + 6, block.GLASS)

# First one torch at each corner
mc.setBlock(housePosition.x + 1, housePosition.y , housePosition.z + 1, block.TORCH)
mc.setBlock(housePosition.x + 1, housePosition.y , housePosition.z + 5, block.TORCH)
mc.setBlock(housePosition.x + 5, housePosition.y , housePosition.z + 1, block.TORCH)
mc.setBlock(housePosition.x + 5, housePosition.y , housePosition.z + 5, block.TORCH)

# A chest and a crafting table
mc.setBlock(housePosition.x + 2, housePosition.y , housePosition.z + 5, block.CRAFTING_TABLE)
mc.setBlock(housePosition.x + 3, housePosition.y , housePosition.z + 5, block.CHEST)